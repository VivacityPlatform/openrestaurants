# README #

### What is this repository for? ###

* Open Restaurant Curbs
* Data used for Vivacity�s OpenRestaurant Blog 
* v0.1

### How do I get set up? ###

You can download the data as a shapefile to be used in any ArcGIS environment or you can download the geojson to host it on your own and play with the data in a web environment.


### Who do I talk to? ###

* For any questions regarding the data set, please contact Hector Tarrido-Picart at: hector@vivacity.city

### License ###

Copyright 2020 Vivacity, Inc.

Permission is hereby granted, free of charge, to any person obtaining a copy of this data and associated documentation files (the "data"), to deal in the data without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the data, and to permit persons to whom the data is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the data.

THE data IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE data OR THE USE OR OTHER DEALINGS IN THE data.